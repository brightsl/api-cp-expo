# Nodejs API Boiler-Template base on Express

## Introduction
This project was created for api development and Generate by Express-Generator.

## Library
1. express
2. express-validator
3. sequelize
4. dotenv
5. bcrypt

## Development Mode

```bash
npm run dev
```

## Production Mode

```bash
npm start
```

## ENV
.env.example is ENV file to make your own env file
create .env file by

```bash
cp .env.example .env
```