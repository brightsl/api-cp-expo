const express = require('express');
const router = express.Router();

const uc = require("../controllers/userController");

/* GET users listing. */
router.post('/register', uc.register);

router.get('/login', uc.login)

module.exports = router;
