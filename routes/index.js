const express = require('express');
const router = express.Router();

/* GET home page. */
router.get('/ping', function(req, res, next) {
  return res.status(200).json({ ping: 'pong' });
});

module.exports = router;
